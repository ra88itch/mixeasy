<footer class="container-fluid" id="footer">
	<div class="container">

		<div class="row">
			<div class="col-12 footer-address">
				บริษัท มิกซ์-อีซี่ จำกัด (สำนักงานใหญ่)<br>
				219 ถนนประชาอุทิศ แขวงห้วยขวาง <br>
				เขตห้วยขวาง กรุงเทพมหานคร<br>
				โทรศัพท์ 061 453 3669<br><br>
				เวลาทำการ 08:00 - 17:00 น.
			</div>
		</div>
<br>
		<div class="row">
			<div class="col-12">
				<a href="https://facebook.com/mixezy" target="_blank">
					<img class="img-fluid img-social-footer" src="assets/images/icon-social-facebook.png" alt="">
				</a>
				<a href="https://www.facebook.com/messages/t/mixezy" target="_blank">
					<img class="img-fluid img-social-footer" src="assets/images/icon-social-messenger.png" alt="">
				</a>
				<a href="http://line.me/ti/p/~@mixezy" target="_blank">
					<img class="img-fluid img-social-footer" src="assets/images/icon-social-line.png" alt="">
				</a>
			</div>
		</div>

	</div>
</footer>

<section class="container-fluid" id="copyright">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<p><strong>©2019 - Mix-Easy. All Rights Reserved.</strong></p>
			</div>
		</div>
	</div>
</section>

<section class="container-fluid talk-with-me">
	<div class="container">
		<div class="row">
			<div class="col-10 offset-2 col-sm-6 offset-sm-6 col-lg-3 offset-lg-9">
				<div class="talk-with-me-area">
					<div class="talk-with-me-header">
					คำนวณปริมาณคอนกรีต
					</div>
					<div class="talk-with-me-content">
						<div class="row">							
							<div class="col-12">
								คอนกรีตผสมเสร็จมาตรฐาน จัดส่งเป็น คิว หรือ ลูกบาศก์เมตร การคำนวณจึงต้องคำนวณเป็นพื้นที่เท่าไหร่ แล้วหาความหนาโดยเฉลี่ย แล้วเราจะทราบว่าต้องใช้ปูนเท่าไหร่
							</div>
						</div>
						<div class="row">
							<div class="col-8">
								<input class="calc" id="calc_length" placeholder="ความกว้าง">
							</div>
							<div class="col-4 text-center">
								เมตร
							</div>
						</div>
					
						<div class="row">
							<div class="col-8">
								<input class="calc" id="calc_width" placeholder="ความยาว">
							</div>
							<div class="col-4 text-center">
								เมตร
							</div>
						</div>
						
						<div class="row">
							<div class="col-8">
								<input class="calc" id="calc_depth" placeholder="ความหนา">
							</div>
							<div class="col-4 text-center">
								มิลลิเมตร
							</div>
						</div>
						
						<div class="row" style="margin-bottom:15px;">
							<div class="col-8">
								คุณจะต้องใช้คอนกรีตจำนวน
							</div>
							<div class="col-4">
								<span id="calc_result">0</span> คิว
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
/*
<section class="container-fluid talk-with-me">
	<div class="container">
		<div class="row">
			<div class="col-8 offset-4 col-sm-6 offset-sm-6 col-lg-3 offset-lg-9">
				<div class="talk-with-me-area">
					<div class="talk-with-me-header">
						คุยกับ MIX-EASY
					</div>
					<div class="talk-with-me-content">
						สนใจสอบถาม<br>
						<img class="img-fluid" src="assets/images/qrcode-line.png" alt=""><br>
						Add Line<br>
						Line ID: @mixezy<br>
						โทร 061 453 3669<br>

						<span>เวลาทำการ<br>08:00 - 17:00 น.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
*/
?>