<?php
// http://text-dev.codesmash.co.th/sustainability-demo/
?><!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- awesone fonts css-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!-- owl carousel css-->
    <link rel="stylesheet" href="assets/owl-carousel/assets/owl.carousel.min.css" type="text/css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- custom CSS -->
    <link rel="stylesheet" href="assets/css/mixeasy.css?<?php echo DATE('his'); ?>">
    <title>มิกซ์-อีซี่</title>
</head>
<body>

<?php 

$this->load->view('_nav'); 
/*
<div class="container-fluid banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <h1 class="text-right text-bold oversize-xxl"><span class="text-ci-blue">ปูนสั่งง่าย</span><span class="text-light">เช็คราคาทันที</span><br>061 456 3669</h1>
			</div>
        </div>
    </div>
</div>

<div class="container-fluid banner-area-2">
	<div class="row">
		<div class="col-12" style="padding:0;">
			<img class="img-fluid" src="assets/images/top-banner.jpg" alt="">
		</div>
	</div>
</div>
*/

?>
<div class="container-fluid banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <h1 class="text-right text-bold oversize-xxl"><span class="text-ci-blue">ปูนสั่งง่าย</span><span class="text-light">เช็คราคาทันที</span><br>061 456 3669</h1>
			</div>
        </div>
    </div>
</div>


<div class="container-fluid feature-list-area" id="why-mix-easy">
    <div class="container">
        <div class="row">
            <div class="offset-md-1 col-md-10 text-center">
                <h1 class="text-bold oversize-xxl">ทำไมต้องมิกซ์-อีซี่</h1>
			</div>
        </div>
        <div class="row" style="padding:50px 0;">
            <div class="col-6 col-md-3 text-center">
				<img class="img-fluid rounded-circle" src="assets/images/icon-feature-easy.png" alt="">
				<h2 class="text-bold">ใช้ง่าย</h2>
				<h3>สั่งปูนได้ผ่านมือถือ</h3>
			</div>
            <div class="col-6 col-md-3 text-center">
				<img class="img-fluid rounded-circle" src="assets/images/icon-feature-comfort.png" alt="">
				<h2 class="text-bold">สะดวก</h2>
				<h3>ประหยัดเวลาไม่ต้องเดินทางไปสั่งซื้อ</h3>      
			</div>
            <div class="col-6 col-md-3 text-center">
				<img class="img-fluid rounded-circle" src="assets/images/icon-feature-responsibility.png" alt="">
				<h2 class="text-bold">ตอบไว</h2>
				<h3>เจ้าหน้าที่ืประสานงาน เพื่อให้ได้ข้อมูลที่ต้องการ</h3>
			</div>
            <div class="col-6 col-md-3 text-center">
				<img class="img-fluid rounded-circle" src="assets/images/icon-feature-sure.png" alt="">
				<h2 class="text-bold">ชัวร์</h2>
				<h3>ได้รับปูนอย่างแน่นอน</h3>
			</div>
        </div>
    </div>
</div>


<div class="container-fluid feature-product" id="feature-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 text-center my-auto">
                <h1 class="text-bold oversize-xxl align-middle">เลือกปูน</h1>
			</div>
            <div class="col-md-9 text-center">

				<div class="row">
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-cpac.png" alt="">
					</div>
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-eagle.png" alt="">
					</div>
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-tpi.png" alt="">
					</div>
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-cnh.png" alt="">
					</div>
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-lotus.png" alt="">
					</div>
					<div class="col-4 col-md-4 text-center">
						<img class="img-fluid rounded-circle" src="assets/images/logo-brand-qmix.png" alt="">
					</div>
				</div> 
				
			</div>
        </div>
    </div>
</div>


<div class="container-fluid feature-contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="offset-md-1 col-md-10 text-center">
                <h1 class="text-bold oversize-xxl">ช่องทางการสั่งปูนกับมิกซ์-อีซี่</h1>
			</div>
        </div>
        <div class="row" style="padding:50px 0;">
            <div class="col-12 col-md-4 text-center">
				<a href="tel:+66614533669" target="_blank">
					<img class="img-fluid rounded-circle" src="assets/images/icon-contact-callcenter.png" alt="">
				</a>
				<h2 class="text-bold">คอลเซ็นเตอร์<br>โทร 061 453 3669</h2>
			</div>
            <div class="col-12 col-md-4 text-center">				
				<a href="https://facebook.com/mixezy" target="_blank">
					<img class="img-fluid rounded-circle" src="assets/images/icon-contact-facebook.png" alt="">
				</a>
				<h2 class="text-bold">facebook.com/mixezy</h2> 
			</div>
            <div class="col-12 col-md-4 text-center">
				<a href="http://line.me/ti/p/~@mixezy" target="_blank">
					<img class="img-fluid rounded-circle" src="assets/images/icon-contact-line.png" alt="">
				</a>
				<h2 class="text-bold">Line @mixezy</h2>
			</div>
        </div>
    </div>
</div>
<?php $this->load->view('_footer'); ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-3.3.1.slim.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carousel js-->
<script src="assets/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/js/main.js?<?php echo DATE('his'); ?>"></script>
</body>
</html>
