<nav class="navbar navbar-expand-lg navbar-light bg-light bg-transparent" id="main-nav">
	<a class="navbar-brand">
		<img class="card-img-top img-fluid" src="assets/images/logo.png" alt="MIX EASY">
	</a>
	<button class="navbar-toggler" data-target="#my-nav" onclick="myFunction(this)" data-toggle="collapse">
		<span class="bar1"></span>
		<span class="bar2"></span>
		<span class="bar3"></span>
	</button>
	<div id="my-nav" class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li class="nav-item"><a class="nav-link active" href="#why-mix-easy">ทำไมต้องมิกซ์-อีซี่</a></li>
			<li class="nav-item"><a class="nav-link" href="#feature-product">ปูนของเรา</a></li>
			<li class="nav-item"><a class="nav-link" href="#contact">ช่องทางการติดต่อ</a></li>
		<ul>
	</div>
</nav>