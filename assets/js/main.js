// menu toggle button
function myFunction(x) {
    x.classList.toggle('change');
}

$(document).ready(function(){
	$('.talk-with-me-header').click(function(){
		$('.talk-with-me-content').toggleClass('active');
	});

	calc();
	$('#calc_length, #calc_width, #calc_depth').keyup(function(){
		calc();
	});
	
});

function calc(){
	var calc_length = $('#calc_length').val();
	var calc_width = $('#calc_width').val();
	var calc_depth = $('#calc_depth').val();

	var calc_result = calc_length*calc_width*(calc_depth/1000);
	
	$('#calc_result').html(calc_result);
}